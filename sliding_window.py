from collections import deque

def sliding_window_minimum(arr, k):
    windows = []    # list to hold the minimums of each windows
    
    q = deque()
    
    # Build the initial window
    for i in range(k):
        # while current element is smaller than elements in the deque, pop the deque
        while q and arr[i] <= arr[q[-1]]:
            q.pop()

        # Store the index of current element in the deque
        q.append(i)

    # Building all other windows
    for i in range(k, len(arr)):

        # Append the minimum of current window in windows
        windows.append(arr[q[0]])
        
        # Remove the earlier which are not available in the window
        while q and q[0] <= i - k:
            q.popleft()


        # while current element is smaller than the elements in the deque, pop deque
        while q and arr[i] <= arr[q[-1]]:
            q.pop()

        # append the current index in deque
        q.append(i)

    return windows


print(sliding_window_minimum([1,2,3,6,1,4,7], 3))
            